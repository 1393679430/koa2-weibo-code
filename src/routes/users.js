const router = require('koa-router')()
const jwt = require('jsonwebtoken')
const util = require('util')
const verify = util.promisify(jwt.verify)
const { SECRET } = require('../conf/constant')

router.prefix('/users')

router.get('/', function (ctx, next) {
  ctx.body = 'this is a users response!'
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

router.post('/login', async (ctx, next) => {
  const { userName, password} = ctx.request.body
  let userInfo = null
  if (userName === 'zhangsan' && password === 'abc') {
    userInfo = {
      userId: 1,
      userName,
      nickName: '张三',
    }
  }

  if (!userInfo) {
    return ctx.body = {
      code: -1,
      data: null,
      msg: '登录失败'
    }
  } else {
    let token = jwt.sign(userInfo, SECRET, {expiresIn: '1h'})
    ctx.body = {
      code: 0,
      data: token
    }
  }
})

router.get('/getUserInfo', async (ctx, next) => {
  const token = ctx.header.authorization
  try {
    const userInfo = await verify(token.split(' ')[1], SECRET)
    ctx.body = {
      code: 0,
      data: userInfo
    }
  } catch(error) {
    ctx.body = {
      code: -1,
      data: error
    }
  }
})

module.exports = router
