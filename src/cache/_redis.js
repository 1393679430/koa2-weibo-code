const redis = require('redis')
const { REDIS_CONF } = require('../conf/db')

const redisClient = redis.createClient(REDIS_CONF.prot, REDIS_CONF.host)

redisClient.on('error', error => {
    console.error('redis error: ', error);
})

// set
function set(key, val, timeout = 60 * 60) {
    if (typeof val === 'object') {
        val = JSON.stringify(val)
    }
    redisClient.set(key, val)
    redisClient.expire(key, timeout)
}
// get
function get(key) {
    const promise = new Promise((resolve, reject) => {
        redisClient.get(key, (error, value) => {
            if (error) {
                reject(error)
                return
            }
            if (value === null) {
                resolve(null)
                return
            }
            try {
                resolve(JSON.parse(value))
            } catch(e) {
                resolve(value)
            }
        })
    })
}

module.exports = {
    set, 
    get,
}