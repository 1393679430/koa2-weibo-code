const Sequelize = require('sequelize')
const { MYSQL_CONF } = require('../conf/db')
const { isProd, isTest } = require('../utils/env')
const { host, user, password, database} = MYSQL_CONF
const  config = {
    host,
    dialect: 'mysql'
}

// 单元测试不打印log
if (isTest) {
    config.logging = () => {}
}

 // 线上环境 使用连接池
if (isProd) {
    config.pool = {
        max: 5, // 连接池最大连接数量
        min: 0,
        idle: 1000, // 一个连接池10s之内没有被使用就会释放
    }
}

const seq = new Sequelize(database, user, password, config)

module.exports = seq

// 连侧链接
// seq.authenticate().then(() => {
//     console.log('ok')
// }).catch(error => {
//     console.log(error)
// })