const { User, Blog } = require('./model')

!(async function() {
    // 创建用户
    const zhangsan = await User.create({
        userName: 'zhangsan',
        password: '111',
        nickname: '张三'
    })

    const lisi = await User.create({
        userName: 'lisi',
        password: '222',
        nickname: '李四'
    })

    const blog1 = await Blog.create({
        title: 'blog title1',
        content: '内容一',
        userId: zhangsan.dataValues.id,
    })

    const blog2 = await Blog.create({
        title: 'blog title2',
        content: '内容二',
        userId: zhangsan.dataValues.id,
    })

    const blog3 = await Blog.create({
        title: 'blog title3',
        content: '内容三',
        userId: lisi.dataValues.id,
    })

    const blog4 = await Blog.create({
        title: 'blog title4',
        content: '内容四',
        userId: lisi.dataValues.id,
    })

    console.log('zhangsan', zhangsan.dataValues)
})()