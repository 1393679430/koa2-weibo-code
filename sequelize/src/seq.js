const Sequelize = require('sequelize')

const  config = {
    host: 'localhost',
    dialect: 'mysql'
}

// // 线上环境 使用连接池
// config.pool = {
//     max: 5, // 连接池最大连接数量
//     min: 0,
//     idle: 1000, // 一个连接池10s之内没有被使用就会释放
// }
const seq = new Sequelize('koa2_weibo_db', 'root', 'xld340826', config)

module.exports = seq

// 连侧链接
// seq.authenticate().then(() => {
//     console.log('ok')
// }).catch(error => {
//     console.log(error)
// })