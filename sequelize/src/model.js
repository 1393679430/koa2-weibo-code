const Sequelize = require('sequelize')
const seq = require('./seq')

// 创建 User 模型 数据表名字是 users
const User = seq.define('user', {
    // id 会自动创建，并设为主键，自增
    userName: {
        type: Sequelize.STRING, // varchar(255)
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    nickname: {
        type: Sequelize.STRING,
        comment: '注释'
    },
});

// 创建Blog模型
const Blog = seq.define('blog', {
    title: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    }
})

// 外健关联 方法一
Blog.belongsTo(User, {
    // 创建外键 Blog.userId -> User.id
    foreignKey: 'userId'
})

// 外健关联 方法二
User.hasMany(Blog, {
    foreignKey: 'userId'
})

module.exports = { 
    User,
    Blog,
}