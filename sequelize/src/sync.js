const seq = require('./seq')
require('./model')

// 连侧链接
seq.authenticate().then(() => {
    console.log('auth ok')
}).catch(error => {
    console.log('auth error:', error)
})

// 执行同步
seq.sync({force: true}).then(() => {
    console.log('执行同步 success')
    process.exit()
})