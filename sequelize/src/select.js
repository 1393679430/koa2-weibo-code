const { User, Blog} = require('./model')

!(async function(){
    // 查询一条记录
    const zhangsan = await User.findOne({
        where: {
            userName: 'zhangsan'
        }
    })
    console.log('zhangsan', zhangsan.dataValues)

    // 查询特定的列
    const zhangsanName = await User.findOne({
        attributes: ['userName', 'nickname'],
        where: {
            userName: 'zhangsan'
        }
    })
    console.log('zhangsanName', zhangsanName.dataValues)

    // 查询一个列表
    const zhangsanBlogList = await Blog.findAll({
        where: {
            userId: 1,
        },
        order: [
            ['id', 'desc']
        ]
    })
    console.log('zhangsanBlogList', zhangsanBlogList.map(blog => blog.dataValues))

    // 分页
    const blogPageList = await Blog.findAll({
        limit: 1,
        offset: 2,
        order: [
            ['id', 'desc']
        ]
    })
    console.log('blogPageList', blogPageList.map(blog => blog.dataValues));

    // 查询总数
    const blogListAndCount = await Blog.findAndCountAll({
        limit: 1,
        offset: 2,
        order: [
            ['id', 'desc']
        ]
    })
    console.log('blogListAndCount', blogListAndCount.count);

    // 联表查询
    const blogListWithUser = await Blog.findAndCountAll({
        order: [
            ['id', 'desc']
        ],
        include: [
            {
                model: User,
                attributes: ['userName', 'nickname'],
                where: {
                    userName: 'zhangsan'
                }
            }
        ]
    })

    console.log('blogListWithUser', blogListWithUser.count, blogListWithUser.rows.map(blog => {
        const blogVal = blog.dataValues
        blogVal.user = blogVal.user.dataValues
        return blogVal
    }));

})()